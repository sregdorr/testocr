﻿using System;
using System.IO;
using IronOcr;
using System.Threading.Tasks;

namespace TestOcr
{
    class Program
    {
        static void Main(string[] args)
        {
            var ocr = new IronTesseract();

            var filePath = "tiff2pdf.pdf";

            using (var input = new OcrInput())
            {
                input.AddPdf(filePath);
                var result = ocr.Read(input);

                Console.WriteLine(result.Text);
            }
        }
    }
}
